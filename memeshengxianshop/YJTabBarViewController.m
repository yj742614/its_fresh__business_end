//
//  YJTabBarViewController.m
//  Knowthecloud
//
//  Created by windpower on 2017/10/11.
//  Copyright © 2017年 ceshi. All rights reserved.
//

#import "YJTabBarViewController.h"
#import "OrderListViewController.h"
#import "MineViewController.h"
@interface YJTabBarViewController ()
@property (nonatomic,strong) UIButton *tempBtn;
@end

@implementation YJTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //自定义tabBar
    [self customTabBar];
    
}
- (void)customTabBar
{
    // 设置 TabBarItemTestAttributes 的颜色。
    [self setUpTabBarItemTextAttributes];
    // 设置子控制器
    [self setUpChildViewController];
    
}
/**
 *  添加子控制器，我这里值添加了4个，没有占位自控制器
 */

- (void)setUpChildViewController{
    
    [self addOneChildViewController:[[UINavigationController alloc]initWithRootViewController:[[OrderListViewController alloc]init]]
                          WithTitle:@"订单"
                          imageName:@"order"
                  selectedImageName:@"orderSel"];
    
    [self addOneChildViewController:[[UINavigationController alloc]initWithRootViewController:[[MineViewController alloc] init]]
                          WithTitle:@"我的"
                          imageName:@"my"
                  selectedImageName:@"mySel"];
}
/**
 *  添加一个子控制器
 *
 *  @param viewController    控制器
 *  @param title             标题
 *  @param imageName         图片
 *  @param selectedImageName 选中图片
 */

- (void)addOneChildViewController:(UIViewController *)viewController WithTitle:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName{
    
    viewController.view.backgroundColor     = YJColor(255, 135, 110);
    viewController.tabBarItem.title         = title;
    viewController.tabBarItem.image         = [UIImage imageNamed:imageName];
    UIImage *image = [UIImage imageNamed:selectedImageName];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    viewController.tabBarItem.selectedImage = image;
    [self addChildViewController:viewController];
    
}

/**
 *  tabBarItem 的选中和不选中文字属性
 */
- (void)setUpTabBarItemTextAttributes{
    
    // 普通状态下的文字属性
    NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    normalAttrs[NSForegroundColorAttributeName] = [UIColor grayColor];
    
    // 选中状态下的文字属性
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor redColor];
    
    // 设置文字属性
    UITabBarItem *tabBar = [UITabBarItem appearance];
    [tabBar setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    [tabBar setTitleTextAttributes:normalAttrs forState:UIControlStateHighlighted];
    
}

@end
