
//
//  main.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
