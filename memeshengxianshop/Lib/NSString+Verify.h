//
//  NSString+Verify.h
//  LRPDemo
//
//  Created by zhangwei on 15/8/28.
//  Copyright (c) 2015年 zhangwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Verify)
//验证邮箱
- (BOOL)verityEmailAddress;
//验证手机号
- (BOOL)verityTelepone;
//验证身份证
- (BOOL)veritysf;
//验证昵称
-(BOOL)nikiname;
//验证密码
-(BOOL)passWd;
//验证小数
-(BOOL)xiaoshunum;
//验证是否为数字
-(BOOL)isPureNumandCharacters:(NSString *)NumString;
//判断首字母
- (NSString *)firstCharactor:(NSString *)aString;

@end
