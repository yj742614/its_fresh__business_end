//
//  YJAFN.m
//  MaoShengxiandistribution
//
//  Created by 汪剑榜 on 2018/5/9.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "YJAFN.h"
#import <CommonCrypto/CommonDigest.h>
@implementation YJAFN
//https验证
+ (AFSecurityPolicy*)customSecurityPolicy
{
    // /先导入证书
    NSString *cerPath = [[NSBundle mainBundle] pathForResource:certificate ofType:@"cer"];//证书的路径
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    // AFSSLPinningModeCertificate 使用证书验证模式
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    
    // allowInvalidCertificates 是否允许无效证书（也就是自建的证书），默认为NO
    // 如果是需要验证自建证书，需要设置为YES
    securityPolicy.allowInvalidCertificates = NO;
    
    //validatesDomainName 是否需要验证域名，默认为YES；
    //假如证书的域名与你请求的域名不一致，需把该项设置为NO；如设成NO的话，即服务器使用其他可信任机构颁发的证书，也可以建立连接，这个非常危险，建议打开。
    //置为NO，主要用于这种情况：客户端请求的是子域名，而证书上的是另外一个域名。因为SSL证书上的域名是独立的，假如证书上注册的域名是www.google.com，那么mail.google.com是无法验证通过的；当然，有钱可以注册通配符的域名*.google.com，但这个还是比较贵的。
    //如置为NO，建议自己添加对应域名的校验逻辑。
    securityPolicy.validatesDomainName = YES;
    securityPolicy.pinnedCertificates = [NSSet setWithArray:@[certData]];
    return securityPolicy;
}


//单例
+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static YJAFN *instance;
    dispatch_once(&onceToken, ^{
        NSURL *baseUrl = [NSURL URLWithString:@""];
        instance = [[YJAFN alloc] initWithBaseURL:baseUrl];
        // https ssl 验证。
        //        if(openHttpsSSL)
        //        {
        ////            [self customSecurityPolicy];
        //        }
        instance.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[
                                                                                   @"application/json",
                                                                                   @"text/html",
                                                                                   @"text/json",
                                                                                   @"text/plain",
                                                                                   @"text/javascript",
                                                                                   @"text/xml",
                                                                                   @"image/*"
                                                                                   ]];
        
    });
    return instance;
}

- (NSURLSession *)downloadSession
{
    if (_downloadSession == nil) {
        
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        // nil : nil的效果跟 [[NSOperationQueue alloc] init] 是一样的
        _downloadSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    }
    return _downloadSession;
}



//get请求
+ (void)getWithUrl: (NSString *)url params: (NSDictionary *)params isReadCache: (BOOL)isReadCache success: (responseSuccess)success failed: (responseFailed)failed  {
    
    [[YJAFN sharedManager] GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //请求成功的回调
        if (success) {
            success(task,responseObject);
        }
        //请求成功,保存数据
        [YJCache saveDataCache:responseObject forKey:url];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //请求失败的回调
        id cacheData= nil;
        //是否读取缓存
        if (isReadCache) {
            cacheData = [YJCache readCache:url];
        }else {
            cacheData = nil;
        }
        
        if (failed) {
            failed(task,error,cacheData);
        }
        
    }];
}

//post请求
+ (void)postWithUrl:(NSString *)url params:(NSDictionary *)params isReadCache: (BOOL)isReadCache success:(responseSuccess)success failed:(responseFailed)failed {
    [[YJAFN sharedManager] POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(task,responseObject);
        }
        //请求成功,保存数据
        [YJCache saveDataCache:responseObject forKey:url];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        id cacheData= nil;
        //是否读取缓存
        if (isReadCache) {
            cacheData = [YJCache readCache:url];
        }else {
            cacheData = nil;
        }
        
        if (failed) {
            failed(task,error,cacheData);
        }
    }];
}

//文件上传

+ (void)uploadWithUrl: (NSString *)url params: (NSDictionary *)params fileData: (NSData *)fileData name: (NSString *)name fileName: (NSString *)fileName mimeType: (NSString *)mimeType progress: (progress)progress success: (responseSuccess)success failed: (responseFailed)failed {
    [[YJAFN sharedManager] POST:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:fileData name:name fileName:fileName mimeType:mimeType];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress);
        }
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(task,responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failed) {
            failed(task,error,nil);
        }
        
    }];
    
}

//文件下载 支持断点下载
+ (void)downloadWithUrl: (NSString *)url {
    // 1. URL
    NSURL *URL = [NSURL URLWithString:url];
    
    // 2. 发起下载任务
    [YJAFN sharedManager].downloadTask = [[YJAFN sharedManager].downloadSession downloadTaskWithURL:URL];
    
    // 3. 启动下载任务
    [[YJAFN sharedManager].downloadTask resume];
    
}

+(void)AFN:(NSString *)string and:(NSDictionary *)params or:(void (^)(NSDictionary *json))Alertbolck orTwo:(void (^)(NSDictionary *json))quitebolck orthree:(void (^)(NSDictionary *json))errorBlock orfour:(void (^)(NSError *error))errorFauBlock
{
    UIView *rootView = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    MBProgressHUD *hub = [[MBProgressHUD alloc]initWithView:rootView];
    [rootView addSubview:hub];
    hub.labelText = @"加载中...";
    [hub show:YES];
//    NSDictionary *paramsdic = @{
//                                @"base64":[YJAFN DicChangeNSDictionary:params],
//                                };
    [[YJAFN sharedManager].requestSerializer willChangeValueForKey:@"timeoutInterval"];
    [YJAFN sharedManager].requestSerializer.timeoutInterval = 30.f;
    [[YJAFN sharedManager].requestSerializer didChangeValueForKey:@"timeoutInterval"];
    [YJAFN sharedManager].responseSerializer = [AFHTTPResponseSerializer serializer];
    [YJAFN postWithUrl:string params:params isReadCache:NO success:^(NSURLSessionDataTask *task, id responseObject){
        NSDictionary *json=[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        if ([json[@"code"]integerValue] == 0) {
            Alertbolck(json);
        }
        else if([json[@"code"]integerValue] == 100)
        {
            NSDictionary *dictionary = [YJUserDefaults dictionaryRepresentation];
            for(NSString* key in [dictionary allKeys]){
                if ([key  isEqual: @"appVersion"]) {
                    
                }
                else
                {
                    [YJUserDefaults removeObjectForKey:key];
                    [YJUserDefaults synchronize];
                }
            }
            quitebolck(json);
        }
        else
        {
            errorBlock(json);
        }
        [hub hide:YES];
        [hub removeFromSuperview];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        if (errorFauBlock) {
            errorFauBlock(error);
        }
        //请求超时
        if (error.code  == -1001) {
            //3、弹出提示：请求超时
            [TipsView showTipOnKeyWindow:@"请求超时,请刷新后再试！"];
            
        }
        [hub hide:YES];
        [hub removeFromSuperview];
    }];
}



//暂停下载
- (void)pauseDownload {
    [self.downloadTask cancelByProducingResumeData:^(NSData * _Nullable resumeData) {
        self.resumeData = resumeData;
        //将已经下载的数据存到沙盒,下次APP重启后也可以继续下载
        NSString *doc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        // 拼接文件路径   上面获取的文件路径加上文件名
        NSString *path = [@"sssssaad" stringByAppendingString:@".plist"];
        NSString *plistPath = [doc stringByAppendingPathComponent:path];
        self.resumeDataPath = plistPath;
        [resumeData writeToFile:plistPath atomically:YES];
        self.resumeData = resumeData;
        self.downloadTask = nil;
    }];
    
}

//继续下载
- (void)resumeDownloadprogress: (progress)progress success: (downloadSuccess)success failed: (downloadFailed)failed  {
    if (self.resumeData == nil) {
        NSData *resume_data = [NSData dataWithContentsOfFile:self.resumeDataPath];
        if (resume_data == nil) {
            // 即没有内存续传数据,也没有沙盒续传数据,就续传了
            return;
        } else {
            // 当沙盒有续传数据时,在内存中保存一份
            self.resumeData = resume_data;
        }
    }
    
    // 续传数据时,依然不能使用回调
    // 续传数据时起始新发起了一个下载任务,因为cancel方法是把之前的下载任务干掉了 (类似于NSURLConnection的cancel)
    // resumeData : 当新建续传数据时,resumeData不能为空,一旦为空,就崩溃
    // downloadTaskWithResumeData :已经把Range封装进去了
    
    if (self.resumeData != nil) {
        self.downloadTask = [self.downloadSession downloadTaskWithResumeData:self.resumeData];
        // 重新发起续传任务时,也要手动的启动任务
        [self.downloadTask resume];
        
    }
}

#pragma NSURLSessionDownloadDelegate

/// 监听文件下载进度的代理方法
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    // 计算进度
    float downloadProgress = (float)totalBytesWritten / totalBytesExpectedToWrite;
    NSLog(@"%f",downloadProgress);
    
    
}

/// 文件下载结束时的代理方法 (必须实现的)
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location
{
    // location : 文件下载结束之后的缓存路径
    // 使用session实现文件下载时,文件下载结束之后,默认会删除,所以文件下载结束之后,需要我们手动的保存一份
    NSLog(@"%@",location.path);
    
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    // NSString *path = @"/Users/allenjzl/Desktop/ssssss/zzzz.zip";
    // 文件下载结束之后,需要立即把文件拷贝到一个不会销毁的地方
    [[NSFileManager defaultManager] copyItemAtPath:location.path toPath:[path stringByAppendingString:@"/.zzzzzzz.zip"] error:NULL];
    NSLog(@"%@",path);
}


+(NSString *)DicChangeNSDictionary:(NSDictionary *)diction
{
    NSMutableDictionary *Changedic = [[NSMutableDictionary alloc]init];
    [Changedic setObject:@"91gxy" forKey:@"channel"];
    [Changedic setObject:[self getNowTimeTimestamp] forKey:@"timestamp"];
    [Changedic setObject:@"AzkDh5@FnLKCR0UHJyfyYrNRGvYpfz=I" forKey:@"private_key"];
    NSArray *keyArray = [diction allKeys];
    NSArray *sortArray = [keyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortString in sortArray) {
        [valueArray addObject:[diction objectForKey:sortString]];
    }
    for (int i = 0; i<sortArray.count; i++) {
        
        [Changedic setValue:valueArray[i] forKey:sortArray[i]];
    }
    NSArray *YJkeyArray = [Changedic allKeys];
    NSArray *YJsortArray = [YJkeyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSMutableArray *YJvalueArray = [NSMutableArray array];
    for (NSString *sortString in YJsortArray) {
        [YJvalueArray addObject:[Changedic objectForKey:sortString]];
    }
    NSMutableArray *YJsignArray = [NSMutableArray array];
    for (int i = 0; i < YJsortArray.count; i++) {
        NSString *keyValueStr = [NSString stringWithFormat:@"%@=%@",YJsortArray[i],YJvalueArray[i]];
        [YJsignArray addObject:keyValueStr];
    }
    NSString *sign = [YJsignArray componentsJoinedByString:@"&"];
    //    NSLog(@"%@",sign);
    NSString *encodedString = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)sign,
                                                              NULL,
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
    const char* input = [encodedString UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(input, (CC_LONG)strlen(input), result);
    NSMutableString *digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [digest appendFormat:@"%02x", result[i]];
    }
    
    [Changedic removeObjectForKey:@"private_key"];
    [Changedic setObject:digest forKey:@"sign"];
    NSArray *ChangedickeyArray = [Changedic allKeys];
    NSArray *ChangedicsortArray = [ChangedickeyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSMutableArray *ChangedicvalueArray = [NSMutableArray array];
    for (NSString *sortString in ChangedicsortArray) {
        [ChangedicvalueArray addObject:[Changedic objectForKey:sortString]];
    }
    NSMutableArray *ChangedicsignArray = [NSMutableArray array];
    for (int i = 0; i < ChangedicsortArray.count; i++) {
        NSString *keyValueStr = [NSString stringWithFormat:@"%@=%@",ChangedicsortArray[i],ChangedicvalueArray[i]];
        [ChangedicsignArray addObject:keyValueStr];
    }
    
    NSString *Changedicsign = [ChangedicsignArray componentsJoinedByString:@"&"];
    //    NSLog(@"%@",Changedicsign);
    NSData *data = [Changedicsign dataUsingEncoding:NSUTF8StringEncoding];
    NSData *base64Data = [data base64EncodedDataWithOptions:0];
    NSString *baseString = [[NSString alloc]initWithData:base64Data encoding:NSUTF8StringEncoding];
    NSString *UrlString = [NSString stringWithFormat:@"%@",baseString];
    //    NSString *Url = [NSString stringWithFormat:@"%@/%@",YJapiurl,UrlString];
    //    NSLog(@"%@",Url);
    return UrlString;
}

+(NSString *)getNowTimeTimestamp{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    //设置时区,这个对于时间的处理有时很重要
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    
    return timeSp;
    
}

+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha
{
    //删除字符串中的空格
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6)
    {
        return [UIColor clearColor];
    }
    // strip 0X if it appears
    //如果是0x开头的，那么截取字符串，字符串从索引为2的位置开始，一直到末尾
    if ([cString hasPrefix:@"0X"])
    {
        cString = [cString substringFromIndex:2];
    }
    //如果是#开头的，那么截取字符串，字符串从索引为1的位置开始，一直到末尾
    if ([cString hasPrefix:@"#"])
    {
        cString = [cString substringFromIndex:1];
    }
    if ([cString length] != 6)
    {
        return [UIColor clearColor];
    }
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    //r
    NSString *rString = [cString substringWithRange:range];
    //g
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    //b
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float)r / 255.0f) green:((float)g / 255.0f) blue:((float)b / 255.0f) alpha:alpha];
}







@end
