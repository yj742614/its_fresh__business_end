//
//  UILabel+LabelHeightAndWidth.h
//  91gxy
//
//  Created by windpower on 2018/4/10.
//  Copyright © 2018年 windpower. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (LabelHeightAndWidth)
+ (CGFloat)getHeightByWidth:(CGFloat)width title:(NSString *)title font:(UIFont*)font;

+ (CGFloat)getWidthWithTitle:(NSString *)title font:(UIFont *)font;

@end
