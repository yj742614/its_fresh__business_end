//
//  MineViewController.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "MineViewController.h"
#import "LoginViewController.h"
@interface MineViewController ()
@property(nonatomic,strong)UIImageView *headImage;
@property(nonatomic,strong)UILabel *namelabel;
@property(nonatomic,strong)UILabel *moneylabel;
@property(nonatomic,strong)UILabel *moneyTitlabel;
@property (weak, nonatomic) IBOutlet UIButton *Quite;



@end

@implementation MineViewController
-(UIImageView *)headImage
{
    if (!_headImage) {
        _headImage = [[UIImageView alloc]init];
        _headImage.layer.cornerRadius = 40;
        _headImage.clipsToBounds = YES;
        [_headImage sd_setImageWithURL:[NSURL URLWithString:[YJUserDefaults objectForKey:@"merchant_head_picture"]] placeholderImage:[UIImage imageNamed:@"defaulHead"]];
    }
    return _headImage;
}
-(UILabel *)namelabel
{
    if (!_namelabel) {
        _namelabel = [[UILabel alloc]init];
        _namelabel.text = [YJUserDefaults objectForKey:@"merchant_name"];
        _namelabel.textAlignment = NSTextAlignmentCenter;
        _namelabel.textColor = [UIColor whiteColor];
        _namelabel.font = [UIFont systemFontOfSize:13];
    }
    return _namelabel;
}
-(UILabel *)moneylabel
{
    if (!_moneylabel) {
        _moneylabel = [[UILabel alloc]init];
        _moneylabel.text = @"10000000.00";
        _moneylabel.textAlignment = NSTextAlignmentCenter;
         _moneylabel.textColor = [UIColor whiteColor];
        _moneylabel.font = [UIFont systemFontOfSize:15];
    }
    return _moneylabel;
}
-(UILabel *)moneyTitlabel
{
    if (!_moneyTitlabel) {
        _moneyTitlabel = [[UILabel alloc]init];
        _moneyTitlabel.text = @"账户余额(元)";
        _moneyTitlabel.textAlignment = NSTextAlignmentCenter;
         _moneyTitlabel.textColor = [UIColor whiteColor];
        _moneyTitlabel.font = [UIFont systemFontOfSize:13];
    }
    return _moneyTitlabel;
}
-(void)viewDidAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
    [self afnData];
}
-(void)viewDidDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden =YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNav];
    [self initUI];
}
-(void)afnData
{
    NSDictionary *parms =@{
                           @"merchant_id":[YJUserDefaults objectForKey:@"id"],
                           @"token":[YJUserDefaults objectForKey:@"token"],
                           };
    [YJAFN AFN:YJqueryBalanceUrl and:parms or:^(NSDictionary *json) {
        NSLog(@"%@",json);
        _moneylabel.text = json[@"data"][@"current_balance"];
    } orTwo:^(NSDictionary *json) {
        
    } orthree:^(NSDictionary *json) {
        NSLog(@"%@",json[@"message"]);
    }orfour:^(NSError *error) {
        
    }];
}
-(void)initNav
{
    self.title =@"我的";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:18],
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navBackimg"] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}
-(void)initUI
{
    _Quite.backgroundColor = YJColor(240, 240, 240);
    _Quite.clipsToBounds = YES;
    _Quite.layer.cornerRadius = 20;
    [_top addSubview:[self headImage]];
    [_top addSubview:[self namelabel]];
    [_top addSubview:[self moneylabel]];
    [_top addSubview:[self moneyTitlabel]];
    [_headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(_top.mas_centerX);
        make.top.mas_equalTo(60);
        make.height.width.mas_equalTo(80);
    }];
    [_namelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(_headImage.mas_bottom);
        make.height.mas_equalTo(20);
    }];
    [_moneylabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(_namelabel.mas_bottom).mas_offset(30);
        make.height.mas_equalTo(20);
    }];
    [_moneyTitlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(_moneylabel.mas_bottom);
        make.height.mas_equalTo(20);
    }];
}
- (IBAction)ClickQuite:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"是否退出登录？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    NSString *neverButtonTitle = @"取消";
    NSString *okButtonTitle = @"确定";
    // 创建操作
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:okButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSDictionary *dictionary = [YJUserDefaults dictionaryRepresentation];
            for(NSString* key in [dictionary allKeys]){
                [YJUserDefaults removeObjectForKey:key];
                [YJUserDefaults synchronize];
            }
            LoginViewController *login = [[LoginViewController alloc]init];
            self.view.window.rootViewController =login;
        }];
        // 添加操作
        [alert addAction:okAction];
        // 创建操作
        UIAlertAction *neverAction = [UIAlertAction actionWithTitle:neverButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }];
        // 添加操作
        [alert addAction:neverAction];
    // 呈现警告视图
    [self presentViewController:alert animated:YES completion:nil];
  
}

@end
