//
//  headView.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "headView.h"

@implementation headView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, YJWidth/4, 30)];
        _nameLabel.text = @"品名";
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_nameLabel];
        
        _typeLabel = [[UILabel alloc]initWithFrame:CGRectMake(YJWidth/4, 0, YJWidth/4, 30)];
        _typeLabel.text = @"规格";
        _typeLabel.textAlignment = NSTextAlignmentCenter;
        _typeLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_typeLabel];
        
        _numLabel = [[UILabel alloc]initWithFrame:CGRectMake(YJWidth/4*2, 0, YJWidth/4, 30)];
        _numLabel.text = @"数量";
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_numLabel];
        
        _moneyLabel = [[UILabel alloc]initWithFrame:CGRectMake(YJWidth/4*3, 0, YJWidth/4, 30)];
        _moneyLabel.text = @"价格（元）";
        _moneyLabel.textAlignment = NSTextAlignmentCenter;
        _moneyLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_moneyLabel];
    }
    return self;
}

@end
