//
//  OrderModel.h
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/12.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "JSONModel.h"

@protocol OrderCommodity
@end
@interface OrderCommodity: JSONModel
@property (nonatomic, copy) NSString <Optional>*children_order_id;
@property (nonatomic, copy) NSString <Optional>*commodity_describe;
@property (nonatomic, copy) NSString <Optional>*commodity_id;
@property (nonatomic, copy) NSString <Optional>*commodity_name;
@property (nonatomic, copy) NSString <Optional>*commodity_picture;
@property (nonatomic, copy) NSString <Optional>*commodity_specification;
@property (nonatomic, copy) NSString <Optional>*create_time;
@property (nonatomic, copy) NSString <Optional>*delete_flag;
@property (nonatomic, copy) NSString <Optional>*oid;
@property (nonatomic, copy) NSString <Optional>*sale_amount;
@property (nonatomic, copy) NSString <Optional>*sell_specifications;
@property (nonatomic, copy) NSString <Optional>*total_amount;
@property (nonatomic, copy) NSString <Optional>*total_num;
@property (nonatomic, copy) NSString <Optional>*total_weight;
@end

@interface OrderModel : JSONModel
@property(nonatomic,copy)NSString <Optional> *amount_payment;
@property(nonatomic,copy)NSString <Optional> *appraise_flag;
@property(nonatomic,copy)NSString <Optional> *complain_content;
@property(nonatomic,copy)NSString <Optional> *complain_flag;
@property(nonatomic,copy)NSString <Optional> *contact_address;
@property(nonatomic,copy)NSString <Optional> *contact_content;
@property(nonatomic,copy)NSString <Optional> *contact_name;
@property(nonatomic,copy)NSString <Optional> *contact_phone;
@property(nonatomic,copy)NSString <Optional> *create_time;
@property(nonatomic,copy)NSString <Optional> *yid;
@property(nonatomic,copy)NSString <Optional> *merchant_id;
@property(nonatomic,copy)NSString <Optional> *merchant_name;
@property(nonatomic,copy)NSString <Optional> *order_id;
@property(nonatomic,copy)NSString <Optional> *yesorno;
@property(nonatomic,copy)NSString <Optional> *payment_time;
@property(nonatomic,copy)NSString <Optional> *preferential_amount;
@property(nonatomic,copy)NSString <Optional> *remarks;
@property(nonatomic,copy)NSString <Optional> *total_amount;
@property(nonatomic,copy)NSString <Optional> *state;
@property(nonatomic,copy)NSString <Optional> *user_id;
@property (nonatomic, strong) NSArray<OrderCommodity *> *orderCommodity;
@end


