//
//  DetailTableViewCell.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "DetailTableViewCell.h"

@implementation DetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setModel:(OrderCommodity *)model
{
    _model = model;
    _name.text = _model.commodity_name;
    _gg.text = model.commodity_specification;
    _num.text = model.total_num;
    _money.text = model.total_amount;
}
@end
