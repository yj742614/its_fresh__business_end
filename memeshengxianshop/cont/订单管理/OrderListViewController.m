//
//  OrderListViewController.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "OrderListViewController.h"
#import "moreButton.h"
#import "OrderListTableViewController.h"
@interface OrderListViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *confirmedButton;
@property (weak, nonatomic) IBOutlet UIButton *processedButton;
@property (weak, nonatomic) IBOutlet UIButton *shippingButton;
@property (weak, nonatomic) IBOutlet UIButton *allButton;
@property (weak, nonatomic) IBOutlet UIScrollView *Contscroller;
@property (strong, nonatomic)UIViewController *willShowVcOne;
@property (strong, nonatomic)UIViewController *willShowVcTwo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftX;
@end

@implementation OrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNav];
    [self initUI];
    [self setTaget];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refdatascroller) name:@"refdata" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(regisCidupdata) name:@"regisCid" object:nil];
}
-(void)viewDidAppear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
}
-(void)viewDidDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden =YES;
}
-(void)setTaget
{
    [_confirmedButton addTarget:self action:@selector(setLeft:) forControlEvents:UIControlEventTouchUpInside];
    [_processedButton addTarget:self action:@selector(setLeft:) forControlEvents:UIControlEventTouchUpInside];
    [_shippingButton addTarget:self action:@selector(setLeft:) forControlEvents:UIControlEventTouchUpInside];
    [_allButton addTarget:self action:@selector(setLeft:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)setLeft:(UIButton *)btn
{
    CGFloat left = btn.frame.origin.x;
    _leftX.constant = left;
    CGPoint offset = self.Contscroller.contentOffset;
    offset.x = (btn.tag-100) * YJWidth;
    [self.Contscroller setContentOffset:offset animated:YES];
}
-(void)initNav
{
    self.title =@"订单管理";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:18],
       NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navBackimg"] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
   
}
-(void)regisCidupdata
{
        [self cidupdata];
}
-(void)refdatascroller
{
    [_Contscroller setContentOffset:CGPointMake(0,0) animated:YES];
}
-(void)initUI
{
    _Contscroller.delegate = self;
    [self setupChildVc];
    self.Contscroller.contentSize = CGSizeMake(4 * YJWidth, 0);
    [self scrollViewDidEndScrollingAnimation:self.Contscroller];
}
-(void)setupChildVc
{
    //添加自控制器
    OrderListTableViewController *yjconter0 = [[OrderListTableViewController alloc]init];
    [self addChildViewController:yjconter0];
    OrderListTableViewController *yjconter1 = [[OrderListTableViewController alloc]init];
    [self addChildViewController:yjconter1];
    OrderListTableViewController *yjconter2 = [[OrderListTableViewController alloc]init];
    [self addChildViewController:yjconter2];
    //    去除需要显示的控制器
    _willShowVcOne = self.childViewControllers[0];
    [self.Contscroller addSubview:_willShowVcOne.view];
    _willShowVcOne.view.hidden = YES;
    //    去除需要显示的控制器
    _willShowVcTwo = self.childViewControllers[1];
    [self.Contscroller addSubview:_willShowVcTwo.view];
    _willShowVcTwo.view.hidden = YES;
}
#pragma UIScrollViewDelegate
//结束动画会调用这个方法
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    CGFloat width = scrollView.frame.size.width;
    CGFloat height =scrollView.frame.size.height;
    CGFloat offsetx =scrollView.contentOffset.x ;
    //    新控制器的索引
    NSInteger index = offsetx / width;
    UIButton *button   = (UIButton *)[self.view viewWithTag:100+index];
    button.selected = YES;
    for (int i = 100; i<=103; i++) {
        UIButton *btn = (id)[self.view viewWithTag:i];
        if (button != btn) {
            btn.selected = NO;
        }
    }
    CGFloat left = button.frame.origin.x;
    _leftX.constant = left;
    NSArray *array = @[@"2",@"4",@"5",@"9"];
    NSNotification *noti = [[NSNotification alloc]initWithName:@"changeorderState" object:self userInfo:@{@"key":array[index]}];
    [[NSNotificationCenter defaultCenter]postNotification:noti];
    if(index/2 == 0)
    {
        if(index == 0)
        {
            _willShowVcOne.view.hidden = NO;
            _willShowVcOne.view.frame = CGRectMake(offsetx, 0, width, height);
        }
        else
        {
            _willShowVcTwo.view.hidden = NO;
            _willShowVcTwo.view.frame = CGRectMake(offsetx, 0, width, height);
        }
    }
    if(index%2 == 0)
    {
        _willShowVcOne.view.hidden = NO;
        _willShowVcOne.view.frame = CGRectMake(offsetx, 0, width, height);
    }
    else
    {
        _willShowVcTwo.view.hidden = NO;
        _willShowVcTwo.view.frame = CGRectMake(offsetx, 0, width, height);
    }
}
//人为操作手指松开scrollerview  scrollerview停止减速会调用这个方法
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollViewDidEndScrollingAnimation:scrollView];
}
-(void)cidupdata
{
    NSDictionary *parms =@{
                           @"merchant_id":[YJUserDefaults objectForKey:@"id"],
                           @"token":[YJUserDefaults objectForKey:@"token"],
                           @"push_id":[YJUserDefaults objectForKey:@"cid"],
                           @"device_number":@"",
                           @"type":@"2",
                           };
    NSLog(@"%@",parms);
    [YJAFN AFN:YJaddCidUrl and:parms or:^(NSDictionary *json) {
        NSLog(@"%@",json);
//        [TipsView showTipOnKeyWindow:json[@"message"]];
    } orTwo:^(NSDictionary *json) {
    } orthree:^(NSDictionary *json) {
        [TipsView showTipOnKeyWindow:json[@"message"]];
    }orfour:^(NSError *error) {
        
    }];
}
@end
