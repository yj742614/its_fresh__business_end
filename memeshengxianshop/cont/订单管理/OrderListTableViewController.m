//
//  OrderListTableViewController.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "OrderListTableViewController.h"
#import "OrderListTableViewCell.h"
#import "OrderModel.h"
@interface OrderListTableViewController ()
@property(nonatomic,assign)NSInteger orderState;
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,assign)NSInteger size;
@property(nonatomic,strong)NSMutableArray *orderListArray;
@property(nonatomic,strong)UIView *noView;
@end

@implementation OrderListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderListTableViewCell" bundle:nil] forCellReuseIdentifier:@"OrderListTableViewCell"];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.showsHorizontalScrollIndicator = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    [self initData];
    [self afnData];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //    // 下拉刷新
    self.tableView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self.orderListArray removeAllObjects];
        [self afnData];
    }];
    //    // 设置自动切换透明度(在导航栏下面自动隐藏)
    self.tableView.mj_header.automaticallyChangeAlpha = YES;
    
    // 上拉刷新
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        self.page++;
        [self afnData];
    }];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeorderState:) name:@"changeorderState" object:nil];
}
-(void)changeorderState:(NSNotification *)title
{
    _orderState =[title.userInfo[@"key"] integerValue];
    [_orderListArray removeAllObjects];
    [self afnData];
}
-(void)initData
{
    _orderState = 2;
    _page = 1;
    _size = 10;
    _orderListArray = [[NSMutableArray alloc]init];
    _noView = [UIView new];
    _noView.frame = CGRectMake(YJWidth*0.5-100, 100, 200, 220);
    UIImageView *noDataView = [UIImageView new];
    noDataView.image = [UIImage imageNamed:@"无订单"];
    noDataView.frame = CGRectMake(0, 0, 200, 200);
    [_noView addSubview:noDataView];
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(0, 200, 200, 20);
    label.text = @"当前没有订单哟";
    label.font = [UIFont systemFontOfSize:16];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor grayColor];
    [_noView addSubview:label];
    
    [self.tableView addSubview:_noView];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return _orderListArray.count;
//    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderListTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"OrderListTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.orderState = _orderState;
    cell.model = _orderListArray[indexPath.section];
    __weak typeof(self) weakSelf = self;
    cell.clickdetail = ^(OrderModel *rowmoder) {
        [weakSelf.orderListArray replaceObjectAtIndex:indexPath.section withObject:rowmoder];
        [weakSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    };
    
    cell.clickOver = ^(OrderModel *rowmoder) {
        [weakSelf over:rowmoder];
    };
    return cell;
}
-(void)over:(OrderModel *)model
{
    __weak typeof(self) weakSelf = self;
    NSDictionary *parms =@{
                           @"merchant_id":[YJUserDefaults objectForKey:@"id"],
                           @"ordernum":model.yid,
                           @"token":[YJUserDefaults objectForKey:@"token"],
                           };
    [YJAFN AFN:YJconfirmOrderUrl and:parms or:^(NSDictionary *json) {

        weakSelf.page = 1;
        [self.orderListArray removeAllObjects];
        [weakSelf afnData];
        
    } orTwo:^(NSDictionary *json) {
    } orthree:^(NSDictionary *json) {
        [TipsView showTipOnKeyWindow:json[@"message"]];
    }orfour:^(NSError *error) {
        
    }];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderModel *order = _orderListArray[indexPath.section];
    CGFloat adressHeight = [UILabel getHeightByWidth:YJWidth-70 title:order.contact_address font:[UIFont systemFontOfSize:13]];
    if (adressHeight < 20) {
        adressHeight = 20;
    }
    else
    {
        adressHeight = adressHeight+2;
    }
    CGFloat noteHeight = [UILabel getHeightByWidth:YJWidth-45 title:order.remarks font:[UIFont systemFontOfSize:13]];
    if (noteHeight < 20) {
        noteHeight = 20;
    }
    else
    {
        noteHeight = noteHeight+2;
    }
    if (_orderState == 2) {
        if ([order.yesorno  isEqual:@"yes"]) {
            return 150+adressHeight+noteHeight+order.orderCommodity.count*30+30;
        }
        else
        {
            return 150+adressHeight+noteHeight;
        }
        
    }
    else
    {
        if ([order.yesorno  isEqual:@"yes"]) {
            return 130+adressHeight+noteHeight+order.orderCommodity.count*30+30;
        }
        else
        {
            return 130+adressHeight+noteHeight;
        }
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = YJColor(240, 240, 240);
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = YJColor(240, 240, 240);
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}
-(void)afnData
{
    NSDictionary *parms =@{
                           @"merchant_id":[YJUserDefaults objectForKey:@"id"],
                           @"orderstate":[NSString stringWithFormat:@"%ld",(long)_orderState],
                           @"token":[YJUserDefaults objectForKey:@"token"],
                           @"page":[NSString stringWithFormat:@"%ld",(long)_page],
                           @"size":[NSString stringWithFormat:@"%ld",(long)_size],
                           };
//        NSDictionary *parms =@{
//                               @"merchant_id":@"1",
//                               @"orderstate":@"5",
//                               @"token":@"62D1138FDA8DCB92AC00E0A73D74BCF8",
//                               @"page":[NSString stringWithFormat:@"%ld",(long)_page],
//                               @"size":[NSString stringWithFormat:@"%ld",(long)_size],
//                               };
    [YJAFN AFN:YJorderlistUrl and:parms or:^(NSDictionary *json) {
//                NSLog(@"%@",json);
        NSArray *array = [OrderModel arrayOfModelsFromDictionaries:json[@"data"] error:nil];
        [self.orderListArray addObjectsFromArray:array];
        if(self.orderListArray.count == 0)
        {
            self.noView.hidden = NO;
        }
        else
        {
            self.noView.hidden = YES;
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } orTwo:^(NSDictionary *json) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
    } orthree:^(NSDictionary *json) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }orfour:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}
@end
