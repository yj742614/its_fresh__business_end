//
//  OrderListTableViewCell.h
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"
#import "moreButton.h"
#import "headView.h"
#import "DetailTableViewCell.h"
typedef void(^clickdetail) (OrderModel *rowmoder);
typedef void(^clickOver) (OrderModel *rowmoder);
@interface OrderListTableViewCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *nameLbel;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UILabel *downTime;
@property (weak, nonatomic) IBOutlet UILabel *ordernum;
@property (weak, nonatomic) IBOutlet UILabel *note;
@property (weak, nonatomic) IBOutlet UIButton *endButton;
@property (weak, nonatomic) IBOutlet UILabel *endadress;
@property(nonatomic,assign)NSInteger orderState;
@property(nonatomic,strong)OrderModel *model;
@property (weak, nonatomic) IBOutlet moreButton *dropdown;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adressHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noteHeight;
@property (weak, nonatomic) IBOutlet UITableView *detailTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviheight;
@property (nonatomic,copy)clickdetail clickdetail;
@property (nonatomic,copy)clickOver clickOver;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *adressTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noteTop;


@end
