//
//  moreButton.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "moreButton.h"

@implementation moreButton

+(instancetype)buttonWithType:(UIButtonType)buttonType
{
    moreButton *ccButton = [super buttonWithType:buttonType];
    if (ccButton)
    {
        ccButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        ccButton.imageView.layer.masksToBounds = YES;
    }
    return ccButton;
}

// 返回背景边界（background）
- (CGRect)backgroundRectForBounds:(CGRect)bounds
{
    return bounds;
}

// 返回内容边界 标题、图片、标题与图片之间的间隔(title + image + the image and title separately)
- (CGRect)contentRectForBounds:(CGRect)bounds
{
    return bounds;
}

// 返回标题边界
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 0, contentRect.size.width-16, 20);
}

// 返回图片边界
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(contentRect.size.width-16, 2, 16, 16);
}
@end
