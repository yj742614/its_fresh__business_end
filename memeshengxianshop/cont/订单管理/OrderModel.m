//
//  OrderModel.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/12.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "OrderModel.h"

@implementation OrderModel
+(BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id":@"yid"
                                                       }];
}

-(void)setOrderCommodity:(NSArray<OrderCommodity *> *)orderCommodity
{
    _orderCommodity = [OrderCommodity arrayOfModelsFromDictionaries:orderCommodity error:nil];
}
@end


@implementation OrderCommodity
+(BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id":@"oid"
                                                       }];
}

@end
