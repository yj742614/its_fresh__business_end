//
//  OrderListTableViewCell.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "OrderListTableViewCell.h"

@implementation OrderListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _dropdown.titleLabel.textAlignment = NSTextAlignmentCenter;
    _phoneButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    _endButton.clipsToBounds = YES;
    _endButton.layer.cornerRadius = 10;
    _endButton.layer.borderWidth = 1;
    _endButton.layer.borderColor = YJColor(240, 240, 240).CGColor;
    [self initTableView];
}
-(void)initTableView
{
    _detailTableView.delegate = self;
    _detailTableView.dataSource = self;
    [_detailTableView registerNib:[UINib nibWithNibName:@"DetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"DetailTableViewCell"];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _model.orderCommodity.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"DetailTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = _model.orderCommodity[indexPath.row];

    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    headView *view = [[headView alloc]initWithFrame:CGRectMake(0, 0, YJWidth, 30)];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = YJColor(240, 240, 240);
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.001;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
-(void)setModel:(OrderModel *)model
{
    _model = model;
    if (_orderState != 2||[model.state integerValue] != 2) {
        _endButton.hidden = YES;
    }
    else
    {
        _endButton.hidden = NO;
    }
    _endadress.text = model.contact_address;
    CGFloat adressH =[UILabel getHeightByWidth:YJWidth-70 title:model.contact_address font:_endadress.font];
    if (adressH < 20) {
        _adressHeight.constant = 20;
    }
    else
    {
        _adressHeight.constant = adressH;
        _adressTop.constant = 7;
    }
    _ordernum.text = model.order_id;
    _nameLbel.text = model.contact_name;
    [_phoneButton setTitle:model.contact_phone forState:UIControlStateNormal];
    _downTime.text = model.create_time;
    CGFloat noteH = [UILabel getHeightByWidth:YJWidth-45 title:model.remarks font:_note.font];
    if (noteH < 20) {
        _noteHeight.constant = 20;
    }
    else
    {
        _noteHeight.constant = noteH;
        _noteTop.constant = 7;
    }
    _note.text =model.remarks;
    if ([model.yesorno  isEqual: @"yes"]) {
        _dropdown.selected = YES;
        _tableviheight.constant = _model.orderCommodity.count*30+30;
    }
    else
    {
        _dropdown.selected = NO;
        _tableviheight.constant = 0;
    }
    [_detailTableView reloadData];
}
- (IBAction)clickPhone:(UIButton *)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:[NSString stringWithFormat:@"电话tel:%@",_model.contact_phone] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }];
    [alert addAction:okAction];
    UIAlertAction *canceAction = [UIAlertAction actionWithTitle:@"拨打电话" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSMutableString * callPhone=[[NSMutableString alloc] initWithFormat:@"tel:%@",_model.contact_phone];
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone] options:@{} completionHandler:nil];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone]];
        }
    }];
    [alert addAction:canceAction];
    //初始化UIWindows
    UIWindow *aW = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    aW.rootViewController = [[UIViewController alloc]init];
    aW.windowLevel = UIWindowLevelAlert + 1;
    [aW makeKeyAndVisible];
    [aW.rootViewController presentViewController:alert animated:YES completion:nil];
}
- (IBAction)clickBegintorecover:(moreButton *)sender {
    if (sender.selected == YES) {
        sender.selected = NO;
        _tableviheight.constant = 0;
        _model.yesorno = @"no";
        if (self.clickdetail) {
            self.clickdetail(_model);
        }
    }
    else
    {
        _model.yesorno = @"yes";
        sender.selected = YES;
        _tableviheight.constant = _model.orderCommodity.count*30+30;
        if (self.clickdetail) {
            self.clickdetail(_model);
        }
    }
}
- (IBAction)clickOverOrder:(id)sender {
    if (self.clickOver) {
        self.clickOver(_model);
    }
}



@end
