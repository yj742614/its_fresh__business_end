//
//  LoginViewController.m
//  memeshengxianshop
//
//  Created by 汪剑榜 on 2018/5/11.
//  Copyright © 2018年 汪剑榜. All rights reserved.
//

#import "LoginViewController.h"
#import "YJTabBarViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}
-(void)initUI
{
    _loginButton.clipsToBounds = YES;
    _loginButton.layer.cornerRadius = 20;
    
    UIView *phoneview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *phoneimageStar =[[UIImageView alloc]initWithFrame:CGRectMake(5 , 5 , 20, 20)];
    phoneimageStar.image = [UIImage imageNamed:@"star"];
    [phoneview addSubview:phoneimageStar];
    _phone.leftViewMode = UITextFieldViewModeAlways;
    _phone.leftView = phoneview;
    
    UIView *passwordview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIImageView *passwordimageStar =[[UIImageView alloc]initWithFrame:CGRectMake(5 , 5 , 20, 20)];
    passwordimageStar.image = [UIImage imageNamed:@"star"];
    [passwordview addSubview:passwordimageStar];
    _password.leftViewMode = UITextFieldViewModeAlways;
    _password.leftView = passwordview;
}
- (IBAction)LoginClick:(id)sender {
    if (![_phone.text verityTelepone])
    {
        [TipsView showTipOnKeyWindow:@"请输入正确的手机号"];
    }
    else
    {
        if (![_password.text passWd])
        {
            [TipsView showTipOnKeyWindow:@"请输入正确的密码"];
        }
        else
        {
            NSDictionary *parms =@{
                                   @"login_account":_phone.text,
                                   @"password":_password.text,
                                   };
            [YJAFN AFN:YJloginUrl and:parms or:^(NSDictionary *json) {
                NSLog(@"%@",json);
                NSDictionary *data = json[@"data"];
                [YJUserDefaults setObject:data[@"merchant_head_picture"] forKey:@"merchant_head_picture"];
                [YJUserDefaults setObject:data[@"token"] forKey:@"token"];
                [YJUserDefaults setObject:data[@"id"] forKey:@"id"];
                [YJUserDefaults setObject:data[@"merchant_name"] forKey:@"merchant_name"];
                self.view.window.rootViewController = [[YJTabBarViewController alloc]init];
            } orTwo:^(NSDictionary *json) {
                
            } orthree:^(NSDictionary *json) {
                NSLog(@"%@",json[@"message"]);
            }orfour:^(NSError *error) {
                
            }];

        }
    }
}




@end
